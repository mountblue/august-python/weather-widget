# Weather Widget Webapp
## Jyotirmoy Mandal


Implemented AJAX calls using javascript in this project  
Features:  
- Get temperature for any city around the world
- Displays error message in case an invalid input is made.

---

Access the webapp by opening index.html in a web-browser.