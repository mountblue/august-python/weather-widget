$(function() {
  $("#location").keyup(function(event) {
      if (event.keyCode === 13) {
          $("#go-button").click();
      }
  });
});



function displayTemperature() {
  var location = document.getElementById('location').value;
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var temperature = JSON.parse(request.response);
      document.getElementById("temperature").innerHTML = "City: " + location + "<br>Temperature: " + temperature.main.temp + "&degC";
    }
    if(this.readyState == 4 && this.status == 404) {
      document.getElementById("temperature").innerHTML = "Enter valid location...";
    };
  };
  request.open("GET", "http://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=47b91fd535f72f570e189b73f2e1dd19&units=metric", true);
  request.send();
  document.getElementById("location").value = "";
}


// // Trying to implement ajax with jquery (status: failed)
//
// $("#go-button").click(function() {
//   $.ajax({
//     type: "POST",
//     url: "http://api.openweathermap.org/data/2.5/weather?q=" + $("#location").val() + "&appid=47b91fd535f72f570e189b73f2e1dd19&units=metric",
//     dataType: "json",
//     success: function(result, status, xhr) {
//       var table = $("<table><tr><th>Weather Description</th></tr>");
//       table.append("<tr><td>City:</td><td>" + result["name"] + "</td></tr>");
//       table.append("<tr><td>Country:</td><td>" + result["sys"]["country"] + "</td></tr>");
//       table.append("<tr><td>Current Temperature:</td><td>" + result["main"]["temp"] + "°C</td></tr>");
//       table.append("<tr><td>Humidity:</td><td>" + result["main"]["humidity"] + "</td></tr>");
//       table.append("<tr><td>Weather:</td><td>" + result["weather"][0]["description"] + "</td></tr>");
//
//       $("#temperature").html(table);
//     },
//     error: function(xhr, status, error) {
//       alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
//     }
//   });
// });
